
from django.conf.urls import url
from django_sso.api import views as api_views

urlpatterns = [
    url(r"^sign-on$", api_views.sign_on, name="sign_on"),
    url(r"^acs$", api_views.assertion_consumer_service, name="acs"),
]
