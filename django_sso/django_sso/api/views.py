import logging

from django.conf import settings
from django.http import HttpRequest, HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods

from django_sso.api.formatters import dict_to_table
from django_sso.api.saml import (SamlError, SamlExpired, fetch_auth_data,
                                 get_sso_login_url)

LOGGER = logging.getLogger(__name__)


@require_http_methods(("GET",))
def sign_on(request: HttpRequest) -> HttpResponse:
    LOGGER.debug("Attempts to sign on")

    try:
        login_url = get_sso_login_url(request.GET.get("email", ""))
    except SamlError as e:
        LOGGER.error("Unable to know which IdP to use")

        return HttpResponse(e)

    LOGGER.debug("Redirecting the user to the IdP")

    return HttpResponseRedirect(login_url)


@csrf_exempt
@require_http_methods(("POST",))
def assertion_consumer_service(request: HttpRequest) -> HttpResponse:
    LOGGER.debug("Assertion Consumer Service started")

    try:
        data = fetch_auth_data(request.POST["SAMLResponse"])
        LOGGER.debug(data)
    except SamlExpired as e:
        LOGGER.debug(str(e))
        return HttpResponseRedirect(settings.SSO_LOGIN_URL)
    except SamlError as e:
        LOGGER.debug(str(e))
        return HttpResponse(status=401)

    return HttpResponse(dict_to_table(data))
