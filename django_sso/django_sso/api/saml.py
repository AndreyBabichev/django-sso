from django.conf import settings
from saml2 import entity
from saml2.client import Saml2Client
from saml2.config import Config as Saml2Config
from saml2.validate import ResponseLifetimeExceed, ToEarly


class SamlError(Exception):
    pass


class SamlExpired(SamlError):
    pass


def _get_metadata(conf: dict):
    metadata = '''<?xml version="1.0" encoding="utf-8"?>
<EntityDescriptor ID="{descriptor_id}" entityID="{ipd_entity_id}" xmlns="urn:oasis:names:tc:SAML:2.0:metadata">
    <IDPSSODescriptor protocolSupportEnumeration="urn:oasis:names:tc:SAML:2.0:protocol">
        <KeyDescriptor use="signing">
            <KeyInfo xmlns="http://www.w3.org/2000/09/xmldsig#">
                <X509Data>
                    <X509Certificate>{cert}</X509Certificate>
                </X509Data>
            </KeyInfo>
        </KeyDescriptor>
        <SingleSignOnService Binding="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect" Location="{sso_url}"/>
    </IDPSSODescriptor>
</EntityDescriptor>
    '''.format(
        descriptor_id=conf["DESCRIPTOR_ID"],
        ipd_entity_id=conf["IDP_ENTITY_ID"],
        cert=conf["CERT"],
        sso_url=conf["SSO_URL"],
    )

    return {"inline": [metadata]}


def get_saml_client():
    url = settings.SSO_REPLY_URL
    saml_settings = {
        "metadata": _get_metadata(settings.SAML2_AUTH),
        "service": {
            "sp": {
                "endpoints": {
                    "assertion_consumer_service": [
                        (url, entity.BINDING_HTTP_REDIRECT),
                        (url, entity.BINDING_HTTP_POST)
                    ]
                },
                # Don't verify that the incoming requests originate from us via
                # the built-in cache for authn request ids in pysaml2
                "allow_unsolicited": True,
                "authn_requests_signed": False,
                "logout_requests_signed": True,
                "want_assertions_signed": True,
                "want_response_signed": False,
            }
        },
    }

    if "APP_ENTITY_ID" in settings.SAML2_AUTH:
        saml_settings["entityid"] = settings.SAML2_AUTH["APP_ENTITY_ID"]

    saml_config = Saml2Config()
    saml_config.load(saml_settings)
    saml_config.allow_unknown_attributes = True
    saml_client = Saml2Client(config=saml_config)

    return saml_client


def get_location(http_info):
    """Extract the redirect URL from a pysaml2 http_info object"""
    assert "headers" in http_info
    headers = http_info["headers"]

    assert len(headers) == 1
    header_name, header_value = headers[0]
    assert header_name == "Location"
    return header_value


def get_ava(authn_response, key_name, default=None):
    try:
        key = list(filter(lambda k: k.endswith(key_name), authn_response.ava.keys()))[0]
        return authn_response.ava[key][0]
    except IndexError:
        return default


def get_sso_login_url(email="", relay_state=""):
    cli = get_saml_client()

    try:
        (session_id, result) = cli.prepare_for_authenticate(
            entityid=settings.SAML2_AUTH["IDP_ENTITY_ID"], relay_state=relay_state, binding=entity.BINDING_HTTP_REDIRECT
        )
    except TypeError as e:
        raise SamlError(str(e))

    return get_location(result) + "&login_hint=" + email


def fetch_auth_data(saml_response):
    cli = get_saml_client()
    try:
        authn_response = cli.parse_authn_request_response(saml_response, entity.BINDING_HTTP_POST)
    except KeyError as e:
        raise SamlError("Authentication error")
    except (ResponseLifetimeExceed, ToEarly) as e:
        raise SamlExpired(str(e))

    return authn_response.ava
