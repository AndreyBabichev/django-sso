import six

def dict_to_table(ava, lev=0, width=1):
    # https://github.com/IdentityPython/pysaml2/blob/master/example/sp-wsgi/sp.py
    txt = [f'<table style="width: 100%; border: border: {width}px solid #ddd; border-spacing: 10px; border-collapse: collapse;">\n']
    for prop, valarr in ava.items():
        txt.append("<tr>\n")
        if isinstance(valarr, six.string_types):
            txt.append("<th>%s</th>\n" % str(prop))
            txt.append("<td>%s</td>\n" % valarr)
        elif isinstance(valarr, list):
            i = 0
            n = len(valarr)
            for val in valarr:
                if not i:
                    txt.append("<th rowspan=%d>%s</td>\n" % (len(valarr), prop))
                else:
                    txt.append("<tr>\n")
                if isinstance(val, dict):
                    txt.append("<td>\n")
                    txt.extend(dict_to_table(val, lev + 1, width - 1))
                    txt.append("</td>\n")
                else:
                    txt.append("<td>%s</td>\n" % val)
                if n > 1:
                    txt.append("</tr>\n")
                n -= 1
                i += 1
        elif isinstance(valarr, dict):
            txt.append("<th>%s</th>\n" % prop)
            txt.append("<td>\n")
            txt.extend(dict_to_table(valarr, lev + 1, width - 1))
            txt.append("</td>\n")
        txt.append("</tr>\n")
    txt.append("</table>\n")
    return txt

